package com.sixteeneyes.daligoapi.model.member;

import com.sixteeneyes.daligoapi.entity.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {

    private String username;

    private String password;

    private String name;

    private String phoneNumber;

    private String licenceNumber;

    private LocalDate dateBirth;

    private Double point;


    private MemberItem(Builder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.licenceNumber = builder.licenceNumber;
        this.dateBirth = builder.dateBirth;
        this.point = builder.point;
    }

    public static class Builder implements CommonModelBuilder<MemberItem> {

        private final String username;
        private final String password;
        private final String name;
        private final String phoneNumber;
        private final String licenceNumber;
        private final LocalDate dateBirth;
        private final Double point;


        public Builder(Member member) {
            this.username = member.getUsername();
            this.password = member.getPassword();
            this.name = member.getName();
            this.phoneNumber = member.getPhoneNumber();
            this.licenceNumber = member.getLicenceNumber();
            this.dateBirth = member.getDateBirth();
            this.point = 0D;
        }
        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}

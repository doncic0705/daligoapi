package com.sixteeneyes.daligoapi.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AuthPhoneCompleteRequest {

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;

    @ApiModelProperty(notes = "인증번호", required = true)
    @NotNull
    @Length(min = 6, max = 6)
    private String authNumber;
}

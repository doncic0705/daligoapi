package com.sixteeneyes.daligoapi.model.review;

import com.sixteeneyes.daligoapi.entity.Review;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReviewItem {

    private Long id;

    private String name;

    private String title;

    private String content;

    private String imgAddress;

    private LocalDateTime dateWrite;

    private ReviewItem(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.title = builder.title;
        this.content = builder.content;
        this.imgAddress = builder.imgAddress;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<ReviewItem> {


        private final Long id;
        private final String name;
        private final String title;
        private final String content;
        private final String imgAddress;
        private final LocalDateTime dateWrite;

        public Builder(Review review) {
            this.id = review.getId();
            this.name = review.getName().charAt(0) + "O".repeat(review.getName().length() - 1); // 첫글자를 가져오고 나머지 글자는 "O"로 바꿔준다.
            // charAt(0)은 string으로 저장된 문자열 중 하나라는 뜻 , repeat은 반복하라는 뜻임.
            this.title = review.getTitle();
            this.content = review.getContent();
            this.imgAddress = review.getImgAddress();
            this.dateWrite = LocalDateTime.now();
        }

        @Override
        public ReviewItem build() {
            return new ReviewItem(this);
        }
    }
}

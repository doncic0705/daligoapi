package com.sixteeneyes.daligoapi.controller;

import com.sixteeneyes.daligoapi.model.common.CommonResult;
import com.sixteeneyes.daligoapi.model.member.AuthPhoneCompleteRequest;
import com.sixteeneyes.daligoapi.model.member.AuthPhoneSendRequest;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import com.sixteeneyes.daligoapi.service.member.AuthPhoneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "핸드폰 인증 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/auth-phone")
public class AuthPhoneController {
    private final AuthPhoneService authPhoneService;

    @ApiOperation(value = "인증번호 등록")
    @PostMapping("/auth/start")
    public CommonResult setAuthNumber(@RequestBody @Valid AuthPhoneSendRequest request) {
        authPhoneService.setAuthNumber(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "인증번호 받기")
    @PutMapping("/auth/end")
    public CommonResult putAuthStatus(@RequestBody @Valid AuthPhoneCompleteRequest request) {
        authPhoneService.putAuthStatus(request);

        return ResponseService.getSuccessResult();
    }
}

package com.sixteeneyes.daligoapi.controller;


import com.sixteeneyes.daligoapi.model.common.CommonResult;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardItem;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import com.sixteeneyes.daligoapi.service.kickBoard.KickBoardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Api(tags = "킥보드 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kick-board")
public class KickBoardController {
    private final KickBoardService kickBoardService;

    @PostMapping("/file-upload")
    public CommonResult setKickBoards(@RequestParam("csvFile") MultipartFile csvFile) throws Exception {
        kickBoardService.setKickBoards(csvFile);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "근처 킥보드 리스트")
    @GetMapping("/near")
    public ListResult<KickBoardItem> getNearList(@RequestParam("posX") double posX, @RequestParam("posY") double posY, @RequestParam("distanceKm") int distanceKm) {
        return ResponseService.getListResult(kickBoardService.getNearList(posX, posY, distanceKm), true);
    }
}

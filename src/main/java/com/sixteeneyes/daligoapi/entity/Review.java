package com.sixteeneyes.daligoapi.entity;

import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.model.review.ReviewRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    @Column(nullable = false)
    private String imgAddress;

    @Column(nullable = false)
    private LocalDateTime dateWrite;

    public void putReview(ReviewRequest request) {
        this.name = request.getName();
        this.title = request.getTitle();
        this.content = request.getContent();
        this.imgAddress = request.getImgAddress();
        this.dateWrite = LocalDateTime.now();
    }


    private Review(Builder builder) {
        this.name = builder.name;
        this.title = builder.title;
        this.content = builder.content;
        this.imgAddress = builder.imgAddress;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<Review> {

        private final String name;
        private final String title;
        private final String content;
        private final String imgAddress;
        private final LocalDateTime dateWrite;


        public Builder(ReviewRequest request) {
            this.name = request.getName();
            this.title = request.getTitle();
            this.content = request.getContent();
            this.imgAddress = request.getImgAddress();
            this.dateWrite = LocalDateTime.now();
        }
        @Override
        public Review build() {
            return new Review(this);
        }
    }
}

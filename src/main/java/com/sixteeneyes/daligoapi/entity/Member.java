package com.sixteeneyes.daligoapi.entity;

import com.sixteeneyes.daligoapi.enums.MemberGroup;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.model.member.MemberCreateRequest;
import com.sixteeneyes.daligoapi.model.member.MemberUpdateRequest;
import com.sixteeneyes.daligoapi.model.member.PasswordChangeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;

    @Column(nullable = false, unique = true, length = 30)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 13)
    private String phoneNumber;

    @Column(nullable = false, length = 15)
    private String licenceNumber;

    @Column(nullable = false)
    private LocalDate dateBirth;

    @Column(nullable = false)
    private Double point;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private Boolean isEnabled;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public void putPassword(String password) {this.password = password;}


    public void putMember(MemberUpdateRequest request) {
        this.password = request.getPassword();
        this.name = request.getName();
        this.phoneNumber = request.getPhoneNumber();
        this.licenceNumber = request.getLicenceNumber();
    }

    private Member(MemberBuilder builder) {
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.licenceNumber = builder.licenceNumber;
        this.dateBirth = builder.dateBirth;
        this.point = 0D;
        this.dateCreate = builder.dateCreate;
        this.isEnabled = builder.isEnabled;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final MemberGroup memberGroup;
        private final String username;
        private final String password;
        private final String name;
        private final String phoneNumber;
        private final String licenceNumber;
        private final LocalDate dateBirth;
        private final LocalDateTime dateCreate;
        private final Boolean isEnabled;

        // 빌더에서 회원그룹 따로 받는 이유 : 일반유저가 회원가입시 내가 일반유저다 라고 선택하지 않음.
        // 회원등록은 관리자페이지에서 관리자가 하거나 일반유저가 회원가입하거나.. N개의 경우의 수가 존재함.
        public MemberBuilder(MemberGroup memberGroup, MemberCreateRequest createRequest) {
            this.memberGroup = memberGroup;
            this.username = createRequest.getUsername();
            this.password = createRequest.getPassword();
            this.name = createRequest.getName();
            this.phoneNumber = createRequest.getPhoneNumber();
            this.licenceNumber = createRequest.getLicenceNumber();
            this.dateBirth = createRequest.getDateBirth();
            this.dateCreate = LocalDateTime.now();
            this.isEnabled = true;
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}

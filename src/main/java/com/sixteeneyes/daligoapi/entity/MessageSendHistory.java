package com.sixteeneyes.daligoapi.entity;

import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MessageSendHistory {

    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 발신 번호
    @Column(nullable = false, length = 13)
    private String sendPhoneNumber;

    // 수신 번호
    @Column(nullable = false, length = 13)
    private String receivePhoneNumber;

    // 메세지 내용
    @Column(nullable = false, columnDefinition = "TEXT")
    // 메세지 내용은 글자 수 제한을 두지 않기 위해 columnDefinition = "TEXT"를 사용함.
    private String messageContents;

    // 발송 시간
    @Column(nullable = false)
    private LocalDateTime dateSend;


    private MessageSendHistory(Builder builder) {
        this.sendPhoneNumber = builder.sendPhoneNumber;
        this.receivePhoneNumber = builder.receivePhoneNumber;
        this.messageContents = builder.messageContents;
        this.dateSend = builder.dateSend;
    }

    public static class Builder implements CommonModelBuilder<MessageSendHistory> {

        private final String sendPhoneNumber;
        private final String receivePhoneNumber;
        private final String messageContents;
        private final LocalDateTime dateSend;


        public Builder(String sendPhoneNumber, String receivePhoneNumber, String messageContents) {
            // Builder 안 에 request 를 못넣는 이유 : 인증번호가 변수기 때문에 request 를 받지 못해서 풀어서 받았음.
            this.sendPhoneNumber = sendPhoneNumber;
            this.receivePhoneNumber = receivePhoneNumber;
            this.messageContents = messageContents;
            this.dateSend = LocalDateTime.now();
        }

        @Override
        public MessageSendHistory build() {
            return new MessageSendHistory(this);
        }
    }
}

package com.sixteeneyes.daligoapi.service.message;

import com.sixteeneyes.daligoapi.entity.MessageSendHistory;
import com.sixteeneyes.daligoapi.enums.MessageTemplate;
import com.sixteeneyes.daligoapi.repository.MessageSendHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class MessageService {
    // 용도 : 카톡, 문자 등 메세지를 발송해주는 팀임.
    // 원래는 알리고 api 여기서 호출하고 보낸 발송 내역 저장도 함.

    private final MessageSendHistoryRepository messageSendHistoryRepository;

    public void sendMessage(
            MessageTemplate messageTemplate,
            String sendPhoneNumber,
            String receiverPhoneNumber,
            HashMap<String, String> contents) {
        // map을 사용하려고 할 땐 HashMap< , >을 사용해야 한다 / map은 for문도 돌릴 수 있음.
        // 알리고 api 호출하기
        // MessageSendHistory에 데이터 저장하기

        String messageContentOrigin = messageTemplate.getContents();
        for (String key : contents.keySet()) {
            String keyResult = "#\\{" + key +"\\}" ;
           messageContentOrigin = messageContentOrigin.replaceAll(keyResult, contents.get(key));
        } // 치환 해주는 것

        MessageSendHistory messageSendHistory = new MessageSendHistory.Builder(sendPhoneNumber, receiverPhoneNumber, messageContentOrigin).build();
        messageSendHistoryRepository.save(messageSendHistory);
    }
    // 모든 service는 controller랑만 일하지 않는다.
    // MessageService는 다른 service가 일을 하고 부르는 service임.
}

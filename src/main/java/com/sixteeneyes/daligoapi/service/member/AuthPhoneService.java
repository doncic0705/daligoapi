package com.sixteeneyes.daligoapi.service.member;

import com.sixteeneyes.daligoapi.entity.AuthPhone;
import com.sixteeneyes.daligoapi.enums.MessageTemplate;
import com.sixteeneyes.daligoapi.exception.CMissingDataException;
import com.sixteeneyes.daligoapi.model.member.AuthPhoneCompleteRequest;
import com.sixteeneyes.daligoapi.model.member.AuthPhoneSendRequest;
import com.sixteeneyes.daligoapi.repository.AuthPhoneRepository;
import com.sixteeneyes.daligoapi.service.message.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class AuthPhoneService {

    private final AuthPhoneRepository authPhoneRepository;
    private final MessageService messageService;

    public void setAuthNumber(AuthPhoneSendRequest request) {

        Optional<AuthPhone> authPhoneCheckData = authPhoneRepository.findByPhoneNumber(request.getPhoneNumber());

        if (authPhoneCheckData.isPresent()) throw new CMissingDataException();
        // 데이터가 있으면 데이터 값을 던지라고 이야기 해야함.
        // 이미 인증 진행중인 핸드폰 번호라고 띄어주는 역활임.

        String resultRandomNumber = makeRandomNumber();

        AuthPhone authPhone = new AuthPhone.Builder(request.getPhoneNumber(), makeRandomNumber()).build();
        authPhoneRepository.save(authPhone);


        HashMap<String, String> msgMap = new HashMap<>();
        msgMap.put("인증번호", resultRandomNumber);
        messageService.sendMessage(
                MessageTemplate.TD_8763,
                "010-0000-0000",
                request.getPhoneNumber(),
                msgMap
        );
    }
    private String makeRandomNumber() {

        Random random = new Random();
        int createNum = 0;
        String ranNum = "";
        String resultNum = "";

        for (int i = 0; i < 6; i++) {
            createNum = random.nextInt(9); // 0~9 까지 숫자를 랜덤으로 생성한다.
            ranNum = Integer.toString(createNum); // int에서 string으로 바꿈.
            resultNum += ranNum; // 6자리를 만들기 위해 랜덤숫자를 결과에 저장한다.
        }

        return resultNum;
    }

    public void putAuthStatus(AuthPhoneCompleteRequest request) {
        Optional<AuthPhone> authPhone = authPhoneRepository.findByPhoneNumber(request.getPhoneNumber());

        if (authPhone.isEmpty()) throw new CMissingDataException();
        // 만약 데이터가 없다면 중간에 해커가 데이터를 변조한 것 .. 그러니까 던져야함.

        if (!authPhone.get().getAuthNumber().equals(request.getAuthNumber())) throw new CMissingDataException();
        // 만약 인증번호가 일치하지 않는다면 상태를 바꾸지 못함 . 던져야함.

        AuthPhone authPhoneOrigin = authPhone.get();
        authPhoneOrigin.putComplete();
        authPhoneRepository.save(authPhoneOrigin);
    }
}

package com.sixteeneyes.daligoapi.service.member;

import com.sixteeneyes.daligoapi.entity.Member;
import com.sixteeneyes.daligoapi.exception.CUsernameSignInFailedException;
import com.sixteeneyes.daligoapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final MemberRepository memberRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member member = memberRepository.findByUsername(username).orElseThrow(CUsernameSignInFailedException::new);
        return member;
    }
}

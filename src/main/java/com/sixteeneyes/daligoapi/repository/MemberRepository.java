package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
    long countByUsername(String username);

    Page<Member> findAllByIdGreaterThanEqualOrderByIdDesc(long id , Pageable pageable);

    Optional<Member>findByNameAndPhoneNumber(String name, String phoneNumber);

    Optional<Member> findByUsernameAndNameAndPhoneNumber(String username, String name, String phoneNumber);

}

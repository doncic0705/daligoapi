package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.KickBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KickBoardRepository extends JpaRepository<KickBoard, Long> {
}

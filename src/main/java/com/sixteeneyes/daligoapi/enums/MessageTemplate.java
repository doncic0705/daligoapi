package com.sixteeneyes.daligoapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageTemplate {
    TI_1209("킥보드 이용 시작", true, "성함 : #{성함}\n" +
            "연락처 : #{연락처}\n" +
            "희망지점 : #{지점}\n" +
            "\n" +
            "지원자님께서 신청해주신 연락처로 채용 담당자가 확인 후 연락드릴 예정입니다.\n" +
            "\n" +
            "좋은 인연이 되길 희망합니다.\n" +
            "감사합니다.")
    , TD_8415("비밀번호 변경 완료", false, "비밀번호가 변경되었습니다.")
    , TD_8763("본인 인증 번호", true, "인증번호는 #{인증번호} 입니다.")
    , TD_7735("본인 명의로 등록 된 아이디 안내", true, "본인 명의로 등록된 아이디는 #{아이디} 입니다.")
    , TD_7777("임시 비밀번호 안내", true, "임시비밀번호는 #{임시비번} 입니다.");


    private final String name;
    private final boolean isSendMms; // 대체 문자는 (발송, 발송안함) 두가지로 되어 있기 때문에 boolean을 사용함.
    private final String contents;
}

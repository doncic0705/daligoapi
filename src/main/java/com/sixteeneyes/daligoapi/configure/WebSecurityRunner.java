package com.sixteeneyes.daligoapi.configure;

import com.sixteeneyes.daligoapi.service.member.MemberDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WebSecurityRunner implements ApplicationRunner {
    private final MemberDataService memberDataService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        memberDataService.setFirstMember();
    }
}
